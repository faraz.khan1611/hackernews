This is a Hacker news app that is supposed to display news list and the comments on the same.
The first page displays news list, when one of the items is clicked, it is supposed to show the comments of the selected news.

## About the project
All the data is coming from the web endpoint of hackerNews : https://hacker-news.firebaseio.com/v0/topstories.json
.
The response contains a list of ids for news items.
The ids are then used to fetch individual news item

## Screenshots

**News List**


![Webp.net-resizeimage](/uploads/4daa5485699458d44f00dac93d3b6d62/Webp.net-resizeimage.png)




**News Comments View**



![Webp.net-resizeimage__1_](/uploads/1e18580e8724c177a1a03ebb63a6e0d8/Webp.net-resizeimage__1_.png)


The app has 100% of testable business logic unit test coverage and 75% of total business logic converage


## Notes
1. To test the coverage for the app , Right click on Test package inside project -> click on run "All Test" with coverage
2. The app has efficient data caching implemented to avoid network call if possible.
3. The app uses LiveData for lifecycle aware callbacks.
4. The app has its own implementaion of dependency injection implementation to avoid singleton creations

## Unit test
## Screenshot for code coverage
![Screen_Shot_2019-06-20_at_10.58.18_AM](/uploads/48db04e40e5f468f529d3a29d93a2407/Screen_Shot_2019-06-20_at_10.58.18_AM.png)