package com.newsapp.businessLayer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.newsapp.businessLayer.data.DataRepositoryImpl;
import com.newsapp.businessLayer.data.IDataRepository;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.INetwork;
import com.newsapp.helper.Constants;
import com.newsapp.businessLayer.converter.ConverterImpl;
import com.newsapp.businessLayer.converter.IConverter;
import com.newsapp.businessLayer.network.Error;
import com.newsapp.businessLayer.network.INetworkCallbackInterface;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class InteractorTest {

    private final int mockID = 20070701;
    private final List<Integer> mockCommentslist = Arrays.asList(20071343,20071259,20071253,20071532,20071443,20071233,20071268,20072250,20074095,20071903,20074493,20071686,20071666,20071186,20071472,20071294);
    private final List<Integer> mockidsList = Arrays.asList(20071532,20071443,20071233,20071268,20072250,20074095,20071903,20074493,20071686,20071666,20071186,20071472,20071294,20071343,20071259,20071253);


    @Captor
    private ArgumentCaptor<INetworkCallbackInterface<JSONArray>> callbackJSONArrayCaptor;

    @Captor
    private ArgumentCaptor<INetworkCallbackInterface<JSONObject>> callbackJSONObjectCaptor;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    public void loadNewsList_Success() throws Exception {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        IInteractorInterface interactorLayer = new Interactor(dataRepository);
        doNothing().when(networkLayer).fetchJSONArrayWithURL(anyString(), callbackJSONArrayCaptor.capture());
        interactorLayer.loadNewsList(new InteractorConsumer() {
            @Override
            public void accept(List<Integer> data) {
                assertThat(data.size(),is(equalTo(mockidsList.size())));//Verified the data received by interactor is same as created JSON Array
            }

            @Override
            public void accept(int position, NewsEntity entity) {

            }

            @Override
            public void reject(int position, Error error) {

            }

            @Override
            public void reject(Error error) {

            }
        });
        JSONArray mockArray = prepareIntegerJSONArray();
        doCallRealMethod().when(converter).convertToNewsList(any(JSONArray.class));
        callbackJSONArrayCaptor.getValue().onSuccess(mockArray);//calling network success to verify the happy path
        verify(converter,atLeastOnce()).convertToNewsList(mockArray);
    }

    @Test
    public void loadNewsList_Failure() {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        IInteractorInterface interactorLayer = new Interactor(dataRepository);
        doNothing().when(networkLayer).fetchJSONArrayWithURL(anyString(), callbackJSONArrayCaptor.capture());
        interactorLayer.loadNewsList(new InteractorConsumer() {
            @Override
            public void accept(List<Integer> data) {
            }

            @Override
            public void accept(int position, NewsEntity entity) {

            }

            @Override
            public void reject(int position, Error error) {

            }

            @Override
            public void reject(Error error) {
                assertThat(error.getMessage(),is(equalTo("Test Error")));
            }
        });
        callbackJSONArrayCaptor.getValue().onError(new Error("Test Error"));//calling network success to verify the happy path
        verify(converter,times(0)).convertToNewsList(any(JSONArray.class));
    }

    private JSONArray prepareIntegerJSONArray() throws Exception {
        JSONArray jsonArrayMock =  mock(JSONArray.class);

        doReturn(mockidsList.size())
                .when(jsonArrayMock).length();
        for(int i = 0; i < mockidsList.size();i++)
        {
            doReturn(mockidsList.get(i))
                    .when(jsonArrayMock).getInt(i);//Mocking JSONArray for each index
        }
        return jsonArrayMock;
    }


    @Test
    public void loadNewsEntity_Success() throws Exception {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        IInteractorInterface interactorLayer = new Interactor(dataRepository);
        doNothing().when(networkLayer).fetchJSONWithURL(anyString(), callbackJSONObjectCaptor.capture());
        interactorLayer.getDetailedNewsForPositionAndID(1,mockID,new InteractorConsumer() {
            @Override
            public void accept(List<Integer> data) {
            }

            @Override
            public void accept(int position, NewsEntity entity) {
                assertThat(position,is(1));
                assertThat(entity.getId(),is(equalTo(mockID)));
            }

            @Override
            public void reject(int position, Error error) {

            }

            @Override
            public void reject(Error error) {

            }
        });
        JSONObject jsonObjectMock = prepareNewsEntityJSONObject();
        doCallRealMethod().when(converter).convertToNewsEntity(any(JSONObject.class));
        callbackJSONObjectCaptor.getValue().onSuccess(jsonObjectMock);//calling network success to verify the happy path
        verify(converter,atLeastOnce()).convertToNewsEntity(any(JSONObject.class));
    }

    @Test
    public void loadNewsEntity_Failure() {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        IInteractorInterface interactorLayer = new Interactor(dataRepository);
        doNothing().when(networkLayer).fetchJSONWithURL(anyString(), callbackJSONObjectCaptor.capture());
        interactorLayer.getDetailedNewsForPositionAndID(1,mockID,new InteractorConsumer() {
            @Override
            public void accept(List<Integer> data) {
            }

            @Override
            public void accept(int position, NewsEntity entity) {
            }

            @Override
            public void reject(int position, Error error) {
                assertThat(position,is(1));
                assertThat(error.getMessage(),is(equalTo("Something went wrong")));
            }

            @Override
            public void reject(Error error) {

            }
        });
        doCallRealMethod().when(converter).convertToNewsEntity(any(JSONObject.class));
        callbackJSONObjectCaptor.getValue().onError(new Error("Something went wrong"));//calling network success to verify the happy path
        verify(converter,times(0)).convertToNewsEntity(any(JSONObject.class));
    }

    private JSONObject prepareNewsEntityJSONObject() throws Exception {
        JSONObject jsonObjectMock = mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_BY)).thenReturn(true);
        String mockByline = "tysone";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_BY)).thenReturn(mockByline);

        when(jsonObjectMock.has(Constants.JSON_NEWS_DECENDANTS)).thenReturn(true);
        int mockDescendants = 90;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_DECENDANTS)).thenReturn(mockDescendants);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        JSONArray mockKidsJSONArray = mock(JSONArray.class);

        when(jsonObjectMock.has(Constants.JSON_NEWS_KIDS)).thenReturn(true);
        when(jsonObjectMock.getJSONArray(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);
        when(jsonObjectMock.get(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);

        when(jsonObjectMock.has(Constants.JSON_NEWS_SCORE)).thenReturn(true);
        int mockscore = 126;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_SCORE)).thenReturn(mockscore);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        int mocktime = 1559415907;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_TIME)).thenReturn(mocktime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(true);
        String mockTitle = "Cameras Came to the Newsroom";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TEXT)).thenReturn(true);
        String mockText = "See also <i>Kill or Cure</i>, a comprehensive index of every Daily Mail article claiming that something";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TEXT)).thenReturn(mockText);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TYPE)).thenReturn(true);
        String mocktype = "story";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TYPE)).thenReturn(mocktype);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(true);
        String mockURL = "https://www.nytimes.com/2019/06/01/opinion/surveillance-cameras-work.html";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        when(jsonObjectMock.has(Constants.JSON_NEWS_PARENTID)).thenReturn(true);
        int mockParentID = 20070700;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_PARENTID)).thenReturn(mockParentID);

        doReturn(mockCommentslist.size())
                .when(mockKidsJSONArray).length();
        for(int i = 0; i < mockCommentslist.size(); i++)
        {
            doReturn(mockCommentslist.get(i))
                    .when(mockKidsJSONArray).getInt(i);
        }
        //endregion

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(mockTitle)));
        assertThat(newsEntity.getBy(),is(equalTo(mockByline)));
        String mockHostName = "www.nytimes.com";
        assertThat(newsEntity.getHostname(),is(equalTo(mockHostName)));
        assertThat(newsEntity.getDescendants(),is(equalTo(mockDescendants)));
        assertThat(newsEntity.getId(),is(equalTo(mockID)));
        assertThat(newsEntity.getScore(),is(equalTo(mockscore)));
        assertThat(newsEntity.getText(),is(equalTo(mockText)));
        assertThat(newsEntity.getKids().size(),is(equalTo(mockCommentslist.size())));
        assertThat(newsEntity.getKids().get(0),is(equalTo(mockCommentslist.get(0))));
        return jsonObjectMock;
    }


}
