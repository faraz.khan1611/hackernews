package com.newsapp.businessLayer.converter;

import org.hamcrest.core.Is;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.helper.Constants;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConverterImplTest {

    private final String mockTitle = "Cameras Came to the Newsroom";
    private final String mockByline = "tysone";
    private final int mockDescendants = 90;
    private final int mockID = 20070701;
    private final int mockscore = 126;
    private final String mockHostName = "www.nytimes.com";
    private final String mockText = "See also <i>Kill or Cure</i>, a comprehensive index of every Daily Mail article claiming that something";
    private final List<Integer> mockCommentslist = Arrays.asList(20071343,20071259,20071253,20071532,20071443,20071233,20071268,20072250,20074095,20071903,20074493,20071686,20071666,20071186,20071472,20071294);
    private final List<Integer> mockidsList = Arrays.asList(20071532,20071443,20071233,20071268,20072250,20074095,20071903,20074493,20071686,20071666,20071186,20071472,20071294,20071343,20071259,20071253);

    @Test
    public void testConverterImpl_ListFromJSONArray() throws Exception {

        JSONArray jsonArrayMock =  mock(JSONArray.class);

        doReturn(mockidsList.size())
                .when(jsonArrayMock).length();
        for(int i = 0; i < mockidsList.size();i++)
        {
            doReturn(mockidsList.get(i))
                    .when(jsonArrayMock).getInt(i);//Mocking JSONArray for each index
        }

        IConverter IConverter = new ConverterImpl();
        List<Integer> idList = IConverter.convertToNewsList(jsonArrayMock);

        assertThat(idList.size(), Is.is(equalTo(mockidsList.size())));

        assertThat(idList.get(0), Is.is(equalTo(mockidsList.get(0))));//verifying if returned list is similar to JSONArray
    }

    @Test
    public void testConverterImpl_JSONObjectToNewsEntity() throws Exception {

        JSONObject jsonObjectMock =  mock(JSONObject.class);
        prepareNewsEntityJSONArray(jsonObjectMock);

        IConverter IConverter = new ConverterImpl();
        NewsEntity newsEntity = IConverter.convertToNewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(mockTitle)));
        assertThat(newsEntity.getBy(),is(equalTo(mockByline)));
        assertThat(newsEntity.getHostname(),is(equalTo(mockHostName)));
        assertThat(newsEntity.getDescendants(),is(equalTo(mockDescendants)));
        assertThat(newsEntity.getId(),is(equalTo(mockID)));
        assertThat(newsEntity.getScore(),is(equalTo(mockscore)));
        assertThat(newsEntity.getText(),is(equalTo(mockText)));
        assertThat(newsEntity.getKids().size(),is(equalTo(mockCommentslist.size())));
        assertThat(newsEntity.getKids().get(0),is(equalTo(mockCommentslist.get(0))));

    }

    private void prepareNewsEntityJSONArray(JSONObject jsonObjectMock) throws Exception {
        when(jsonObjectMock.has(Constants.JSON_NEWS_BY)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_BY)).thenReturn(mockByline);

        when(jsonObjectMock.has(Constants.JSON_NEWS_DECENDANTS)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_DECENDANTS)).thenReturn(mockDescendants);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        JSONArray mockKidsJSONArray = mock(JSONArray.class);

        when(jsonObjectMock.has(Constants.JSON_NEWS_KIDS)).thenReturn(true);
        when(jsonObjectMock.getJSONArray(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);
        when(jsonObjectMock.get(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);

        when(jsonObjectMock.has(Constants.JSON_NEWS_SCORE)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_SCORE)).thenReturn(mockscore);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        int mocktime = 1559415907;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_TIME)).thenReturn(mocktime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TEXT)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TEXT)).thenReturn(mockText);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TYPE)).thenReturn(true);
        String mocktype = "story";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TYPE)).thenReturn(mocktype);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(true);
        String mockURL = "https://www.nytimes.com/2019/06/01/opinion/surveillance-cameras-work.html";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        when(jsonObjectMock.has(Constants.JSON_NEWS_PARENTID)).thenReturn(true);
        int mockParentID = 20070700;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_PARENTID)).thenReturn(mockParentID);

        doReturn(mockCommentslist.size())
                .when(mockKidsJSONArray).length();
        for(int i = 0; i < mockCommentslist.size(); i++)
        {
            doReturn(mockCommentslist.get(i))
                    .when(mockKidsJSONArray).getInt(i);
        }
        //endregion

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(mockTitle)));
        assertThat(newsEntity.getBy(),is(equalTo(mockByline)));
        assertThat(newsEntity.getHostname(),is(equalTo(mockHostName)));
        assertThat(newsEntity.getDescendants(),is(equalTo(mockDescendants)));
        assertThat(newsEntity.getId(),is(equalTo(mockID)));
        assertThat(newsEntity.getScore(),is(equalTo(mockscore)));
        assertThat(newsEntity.getText(),is(equalTo(mockText)));
        assertThat(newsEntity.getKids().size(),is(equalTo(mockCommentslist.size())));
        assertThat(newsEntity.getKids().get(0),is(equalTo(mockCommentslist.get(0))));
    }

}