package com.newsapp.businessLayer.data;

import com.newsapp.businessLayer.entity.NewsEntity;

import org.json.JSONObject;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class CacheTest {
    private final List<Integer> mockidsList = Arrays.asList(20071532,20071443,20071233,20071268,20072250,20074095,20071903,20074493,20071686,20071666,20071186,20071472,20071294,20071343,20071259,20071253);

    @Test
    public void testGetCachedDataForID_Null()
    {
        ICache cache = new Cache();
        cache.setCacheData(mockidsList);
        assertNull(cache.getCachedDataFor(0));//value will be null as no object is set yet;
    }

    @Test
    public void testGetCachedDataForID_NonNull()
    {
        ICache cache = new Cache();
        cache.setCacheData(mockidsList);
        NewsEntity entity = new NewsEntity(new JSONObject());
        cache.setNewsEntity(mockidsList.get(0),entity);
        assertNotNull(cache.getCachedDataFor(mockidsList.get(0)));//value will be non null as no object is set yet;
    }
}
