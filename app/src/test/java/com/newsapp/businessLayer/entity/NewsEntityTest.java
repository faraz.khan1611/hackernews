package com.newsapp.businessLayer.entity;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import com.newsapp.helper.Constants;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NewsEntityTest {

    private final String mockTitle = "Cameras Came to the Newsroom";
    private final String mockByline = "tysone";
    private final int mockDescendants = 90;
    private final int mockID = 20070701;
    private final int mockParentID = 20070700;
    private final int mockscore = 126;
    private final int mocktime = 1559415907;
    private final String mocktype = "story";
    private final String mockHostName = "www.nytimes.com";
    private final String mockURL = "https://www.nytimes.com/2019/06/01/opinion/surveillance-cameras-work.html";
    private final String mockText = "See also <i>Kill or Cure</i>, a comprehensive index of every Daily Mail article claiming that something";
    private final List<Integer> mockkidslist = Arrays.asList(20071343,20071259,20071253,20071532,20071443,20071233,20071268,20072250,20074095,20071903,20074493,20071686,20071666,20071186,20071472,20071294);

    @Test
    public void testNewsEntity_AllKeyAndValue() throws Exception {
        JSONObject jsonObjectMock =  mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_BY)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_BY)).thenReturn(mockByline);

        when(jsonObjectMock.has(Constants.JSON_NEWS_DECENDANTS)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_DECENDANTS)).thenReturn(mockDescendants);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        JSONArray mockKidsJSONArray = mock(JSONArray.class);

        when(jsonObjectMock.has(Constants.JSON_NEWS_KIDS)).thenReturn(true);
        when(jsonObjectMock.getJSONArray(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);
        when(jsonObjectMock.get(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);

        when(jsonObjectMock.has(Constants.JSON_NEWS_SCORE)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_SCORE)).thenReturn(mockscore);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_TIME)).thenReturn(mocktime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TEXT)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TEXT)).thenReturn(mockText);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TYPE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TYPE)).thenReturn(mocktype);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        when(jsonObjectMock.has(Constants.JSON_NEWS_PARENTID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_PARENTID)).thenReturn(mockParentID);

        doReturn(mockkidslist.size())
                .when(mockKidsJSONArray).length();
        for(int i = 0; i < mockkidslist.size();i++)
        {
            doReturn(mockkidslist.get(i))
                    .when(mockKidsJSONArray).getInt(i);
        }
        //endregion

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(mockTitle)));
        assertThat(newsEntity.getBy(),is(equalTo(mockByline)));
        assertThat(newsEntity.getHostname(),is(equalTo(mockHostName)));
        assertThat(newsEntity.getDescendants(),is(equalTo(mockDescendants)));
        assertThat(newsEntity.getId(),is(equalTo(mockID)));
        assertThat(newsEntity.getScore(),is(equalTo(mockscore)));
        assertThat(newsEntity.getText(),is(equalTo(mockText)));
        assertThat(newsEntity.getKids().size(),is(equalTo(mockkidslist.size())));
        assertThat(newsEntity.getKids().get(0),is(equalTo(mockkidslist.get(0))));
    }

    @Test
    public void testNewsEntity_MissingKeyAndValue_For_Title_URL() throws Exception {
        JSONObject jsonObjectMock =  mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_BY)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_BY)).thenReturn(mockByline);

        when(jsonObjectMock.has(Constants.JSON_NEWS_DECENDANTS)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_DECENDANTS)).thenReturn(mockDescendants);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        JSONArray mockKidsJSONArray = mock(JSONArray.class);

        when(jsonObjectMock.has(Constants.JSON_NEWS_KIDS)).thenReturn(true);
        when(jsonObjectMock.getJSONArray(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);
        when(jsonObjectMock.get(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);

        when(jsonObjectMock.has(Constants.JSON_NEWS_SCORE)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_SCORE)).thenReturn(mockscore);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_TIME)).thenReturn(mocktime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(false);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TEXT)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TEXT)).thenReturn(mockText);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TYPE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TYPE)).thenReturn(mocktype);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(false);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        when(jsonObjectMock.has(Constants.JSON_NEWS_PARENTID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_PARENTID)).thenReturn(mockParentID);

        doReturn(mockkidslist.size())
                .when(mockKidsJSONArray).length();
        for(int i = 0; i < mockkidslist.size();i++)
        {
            doReturn(mockkidslist.get(i))
                    .when(mockKidsJSONArray).getInt(i);
        }
        //endregion

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo("")));
        assertThat(newsEntity.getTimeToDisplay(),is(not("")));
        assertThat(newsEntity.getBy(),is(equalTo(mockByline)));
        assertThat(newsEntity.getHostname(),is(equalTo("")));
        assertThat(newsEntity.getDescendants(),is(equalTo(mockDescendants)));
        assertThat(newsEntity.getId(),is(equalTo(mockID)));
        assertThat(newsEntity.getScore(),is(equalTo(mockscore)));
        assertThat(newsEntity.getText(),is(equalTo(mockText)));
        assertThat(newsEntity.getKids().size(),is(equalTo(mockkidslist.size())));
        assertThat(newsEntity.getKids().get(0),is(equalTo(mockkidslist.get(0))));
    }

    @Test
    public void testNewsEntity_TestCrash_KidsMissing() throws Exception {
        JSONObject jsonObjectMock =  mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_BY)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_BY)).thenReturn(mockByline);

        when(jsonObjectMock.has(Constants.JSON_NEWS_DECENDANTS)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_DECENDANTS)).thenReturn(mockDescendants);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        JSONArray mockKidsJSONArray = mock(JSONArray.class);

        when(jsonObjectMock.has(Constants.JSON_NEWS_KIDS)).thenReturn(false);
        when(jsonObjectMock.getJSONArray(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);
        when(jsonObjectMock.get(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);

        when(jsonObjectMock.has(Constants.JSON_NEWS_SCORE)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_SCORE)).thenReturn(mockscore);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_TIME)).thenReturn(mocktime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TEXT)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TEXT)).thenReturn(mockText);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TYPE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TYPE)).thenReturn(mocktype);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        when(jsonObjectMock.has(Constants.JSON_NEWS_PARENTID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_PARENTID)).thenReturn(mockParentID);

        doReturn(mockkidslist.size())
                .when(mockKidsJSONArray).length();
        for(int i = 0; i < mockkidslist.size();i++)
        {
            doReturn(mockkidslist.get(i))
                    .when(mockKidsJSONArray).getInt(i);
        }
        //endregion

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(mockTitle)));
        assertThat(newsEntity.getBy(),is(equalTo(mockByline)));
        assertThat(newsEntity.getHostname(),is(equalTo(mockHostName)));
        assertThat(newsEntity.getDescendants(),is(equalTo(mockDescendants)));
        assertThat(newsEntity.getId(),is(equalTo(mockID)));
        assertThat(newsEntity.getScore(),is(equalTo(mockscore)));
        assertThat(newsEntity.getText(),is(equalTo(mockText)));
        assertThat(newsEntity.getKids().size(),is(equalTo(0))); // the size is 0 as the kids array is absent
    }

    @Test
    public void testNewsEntity_TestCrash_KidsArrayNotTypeOfJSONArray() throws Exception {
        JSONObject jsonObjectMock =  mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_BY)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_BY)).thenReturn(mockByline);

        when(jsonObjectMock.has(Constants.JSON_NEWS_DECENDANTS)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_DECENDANTS)).thenReturn(mockDescendants);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        JSONArray mockKidsJSONArray = mock(JSONArray.class);

        when(jsonObjectMock.has(Constants.JSON_NEWS_KIDS)).thenReturn(true);
        when(jsonObjectMock.getJSONArray(Constants.JSON_NEWS_KIDS)).thenReturn(mockKidsJSONArray);
        when(jsonObjectMock.get(Constants.JSON_NEWS_KIDS)).thenReturn(new JSONObject());//Returning JSONOBject instead of JsonArray to fail the case

        when(jsonObjectMock.has(Constants.JSON_NEWS_SCORE)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_SCORE)).thenReturn(mockscore);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_TIME)).thenReturn(mocktime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TEXT)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TEXT)).thenReturn(mockText);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TYPE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TYPE)).thenReturn(mocktype);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        when(jsonObjectMock.has(Constants.JSON_NEWS_PARENTID)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_PARENTID)).thenReturn(mockParentID);

        doReturn(mockkidslist.size())
                .when(mockKidsJSONArray).length();
        for(int i = 0; i < mockkidslist.size();i++)
        {
            doReturn(mockkidslist.get(i))
                    .when(mockKidsJSONArray).getInt(i);
        }
        //endregion

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(),is(equalTo(mockTitle)));
        assertThat(newsEntity.getBy(),is(equalTo(mockByline)));
        assertThat(newsEntity.getHostname(),is(equalTo(mockHostName)));
        assertThat(newsEntity.getDescendants(),is(equalTo(mockDescendants)));
        assertThat(newsEntity.getId(),is(equalTo(mockID)));
        assertThat(newsEntity.getScore(),is(equalTo(mockscore)));
        assertThat(newsEntity.getText(),is(equalTo(mockText)));
        assertThat(newsEntity.getKids().size(),is(equalTo(0))); // the size is 0 as the kids array was not of type JSONArray
    }
}
