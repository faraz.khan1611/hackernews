package com.newsapp.helper;

/**
 * Created by Faraz Khan.
 */

public class Constants {
    public final static String NEWS_URL = "https://hacker-news.firebaseio.com/v0/topstories.json";
    public final static String NEWS_DETAILS_FOR_ID_URL = "https://hacker-news.firebaseio.com/v0/item/%d.json";
    public static final String NEWS_ID = "ID";
    public static final String COMMENTS_LIST = "commentsList";

    //endregion

    //region JSON Keys
    public final static String JSON_NEWS_BY = "by";
    public final static String JSON_NEWS_DECENDANTS= "descendants";
    public final static String JSON_NEWS_ID= "id";
    public final static String JSON_NEWS_KIDS= "kids";
    public final static String JSON_NEWS_SCORE= "score";
    public final static String JSON_NEWS_TIME = "time";
    public final static String JSON_NEWS_TITLE = "title";
    public final static String JSON_NEWS_TEXT = "text";
    public final static String JSON_NEWS_TYPE = "type";
    public final static String JSON_NEWS_URL = "url";
    public static final String JSON_NEWS_PARENTID = "parent";
    //endregion


}
