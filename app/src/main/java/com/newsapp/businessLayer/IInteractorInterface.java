package com.newsapp.businessLayer;

import com.newsapp.businessLayer.entity.NewsEntity;
/**
 * IInteractorInterface for layer above data repo for UI specific data manipulation
 * Created by Faraz Khan.
 */
public interface IInteractorInterface {
    void loadNewsList(InteractorConsumer consumer);
    NewsEntity getDetailedNewsForPositionAndID(final int position, int id, InteractorConsumer consumer);
    NewsEntity getDetailedNewsForID(int id);
    }
