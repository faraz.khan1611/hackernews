package com.newsapp.businessLayer.converter;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import com.newsapp.businessLayer.entity.NewsEntity;

/**
 * The Converter interface.
 * Allows object translation
 * Created by Faraz Khan.
 */

public interface IConverter {
    NewsEntity convertToNewsEntity(@NotNull JSONObject result);
    List<Integer> convertToNewsList(@NotNull JSONArray result);
}
