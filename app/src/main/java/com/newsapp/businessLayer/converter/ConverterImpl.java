package com.newsapp.businessLayer.converter;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.newsapp.businessLayer.entity.NewsEntity;

/**
 * The Converter class.
 * Allows object translation
 * Created by Faraz Khan.
 */


public class ConverterImpl implements IConverter {

    @Override
    public NewsEntity convertToNewsEntity(@NotNull JSONObject result){
        NewsEntity entity;
        entity = new NewsEntity(result);
        return entity;
    }

    @Override
    public List<Integer> convertToNewsList(@NotNull JSONArray result) {
        List<Integer> list = new ArrayList<>();
        try {
            for (int i = 0; i < result.length(); i++) {
                list.add(result.getInt(i));
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return list;
    }
}
