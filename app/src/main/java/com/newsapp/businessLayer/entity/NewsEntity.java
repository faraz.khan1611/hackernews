package com.newsapp.businessLayer.entity;

import androidx.annotation.NonNull;

import com.newsapp.businessLayer.utils.JSONUtils;
import com.newsapp.businessLayer.utils.Utils;
import com.newsapp.helper.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * The Entity object.
 * Created by Faraz Khan.
 */


public class NewsEntity{
	private final int score;
	private final String by;
	private final String timeToDisplay;
	private final String title;
	private final String text;

	private final int descendants;
	private final String hostname;
	private final int id;

	private List<Integer> kids;

	public NewsEntity(@NonNull JSONObject jsonObject) {
        score = JSONUtils.getIntFromJSON(jsonObject, Constants.JSON_NEWS_SCORE, 0);
        by = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_NEWS_BY);
		id = JSONUtils.getIntFromJSON(jsonObject, Constants.JSON_NEWS_ID, 0);
		long time = JSONUtils.getLongFromJSON(jsonObject, Constants.JSON_NEWS_TIME, 0);
        title = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_NEWS_TITLE);
		text = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_NEWS_TEXT);
        descendants = JSONUtils.getIntFromJSON(jsonObject, Constants.JSON_NEWS_DECENDANTS, 0);
		String url = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_NEWS_URL);
		long millis = (time * 1000);
		timeToDisplay = Utils.getRelativeTimeSince(millis,System.currentTimeMillis());
		if(url != null && !url.equals("")) {
			URI uri = URI.create(url);
			hostname = uri != null ? uri.getAuthority() : "";
		}
		else
		{
			hostname = "";
		}
        try {
			kids = new ArrayList<>();
			if(jsonObject.has(Constants.JSON_NEWS_KIDS)) {
				if (jsonObject.get(Constants.JSON_NEWS_KIDS) instanceof JSONArray) {
					JSONArray kidsArray = null;
					try {
						kidsArray = jsonObject.getJSONArray(Constants.JSON_NEWS_KIDS);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					if (kidsArray != null) {
						for (int i = 0; i < kidsArray.length(); i++) {
							kids.add(kidsArray.getInt(i));
						}
					}
				}
			}
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

	public int getId() {
		return id;
	}

	public int getScore(){
		return score;
	}

	public String getBy(){
		return by;
	}

	public String getTitle(){
		return title;
	}

	public int getDescendants(){
		return descendants;
	}

	public String getHostname() {
		return hostname;
	}

	public String getTimeToDisplay() {
		return timeToDisplay;
	}

	public List<Integer> getKids() {
		return kids;
	}

	public String getText() {
		return text;
	}

}