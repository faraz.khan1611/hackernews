package com.newsapp.businessLayer;

import java.lang.ref.WeakReference;
import java.util.List;

import com.newsapp.businessLayer.data.IDataRepository;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;

/**
 * IInteractorInterface implementation for layer above data repo for UI specific data manipulation
 * Created by Faraz Khan.
 */

public class Interactor implements IInteractorInterface {

    private final IDataRepository mRepository;

    public Interactor(IDataRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public void loadNewsList(final InteractorConsumer consumer) {
        mRepository.getNewsList(new IDataRepository.IRepoListener<List<Integer>>() {

            final WeakReference<InteractorConsumer> listener = new WeakReference<>(consumer);

            @Override
            public void accept(List<Integer> list) {
                InteractorConsumer cb = listener.get();
                if (cb != null) {
                    cb.accept(list);
                }
            }

            @Override
            public void reject(Error error) {
                InteractorConsumer cb = listener.get();
                if (cb != null) {
                    cb.reject(error);
                }
            }
        });
    }

    //Return NewsEntity from repo,
    //@Return Value is  Null if the file is not in cache, then server sync is triggered
    @Override
    public NewsEntity getDetailedNewsForPositionAndID(final int position, int id, final InteractorConsumer consumer) {
        return mRepository.getNewsEntityForId(id,new IDataRepository.IRepoListener<NewsEntity>() {

            final WeakReference<InteractorConsumer> listener = new WeakReference<>(consumer);

            @Override
            public void accept(NewsEntity entity) {
                InteractorConsumer cb = listener.get();
                if (cb != null) {
                    cb.accept(position,entity);
                }
            }

            @Override
            public void reject(Error error) {
                InteractorConsumer cb = listener.get();
                if (cb != null) {
                    cb.reject(position,error);
                }
            }
        });
    }

    @Override
    public NewsEntity getDetailedNewsForID(int id) {
        return mRepository.getNewsEntityForId(id);
    }


}
