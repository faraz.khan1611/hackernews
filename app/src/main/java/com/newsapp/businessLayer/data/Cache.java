package com.newsapp.businessLayer.data;

import android.annotation.SuppressLint;

import com.newsapp.businessLayer.entity.NewsEntity;

import java.util.HashMap;
import java.util.List;

/**
 * The Cache implementaion.
 * Stores obj for faster access
 * Created by Faraz Khan.
 */

class Cache implements ICache{

    @SuppressLint("UseSparseArrays")
    final private HashMap<Integer, NewsEntity> newsMap = new HashMap<>();

    @Override
    public void setCacheData(List<Integer> list) {
        for(int id:list)
        {
            newsMap.put(id,null);
        }

    }

    @Override
    public void setNewsEntity(int id, NewsEntity entity) {
        newsMap.put(id,entity);
    }

    @Override
    public NewsEntity getCachedDataFor(int id) {
        return newsMap.get(id);
    }
}
