package com.newsapp.businessLayer.data;

import android.annotation.SuppressLint;

import com.newsapp.businessLayer.converter.IConverter;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;
import com.newsapp.businessLayer.network.INetworkCallbackInterface;
import com.newsapp.businessLayer.network.INetwork;
import com.newsapp.helper.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * The Data Repository implementaion.
 * Allows abstraction for Cache and Network
 * Created by Faraz Khan.
 */

public class DataRepositoryImpl implements IDataRepository {

    private final INetwork mNetwork;
    private final IConverter mConverter;
    private final ICache mCache;

    static class FactoryHelper{
        ICache makeCache(){
            return new Cache();
        }
    }

    public DataRepositoryImpl(INetwork network, IConverter converter) {
        this(network, converter,new FactoryHelper());
    }

    public DataRepositoryImpl(INetwork network, IConverter converter,FactoryHelper helper) {
        this.mNetwork = network;
        this.mConverter = converter;
        this.mCache = helper.makeCache();
    }

    @Override
    public void getNewsList(final IRepoListener<List<Integer>> callback) {
        this.mNetwork.fetchJSONArrayWithURL(Constants.NEWS_URL, new INetworkCallbackInterface<JSONArray>() {

            final WeakReference<IConverter> weakConverter = new WeakReference<>(mConverter);
            final WeakReference<IRepoListener<List<Integer>>> listener = new WeakReference<>(callback);

            @Override
            public void onSuccess(JSONArray result) {
                IConverter converter = weakConverter.get();
                if (converter != null) {
                    List<Integer> newsList = converter.convertToNewsList(result);
                    mCache.setCacheData(newsList);
                    IRepoListener<List<Integer>> cb = listener.get();
                    if (cb != null) {
                        cb.accept(newsList);
                    }
                }
            }

            @Override
            public void onError(Error error) {
                IRepoListener cb = listener.get();
                if (cb != null) {
                    cb.reject(error);
                }
            }
        });
    }

    //Return from cache or trigger server call
    @SuppressLint("DefaultLocale")
    @Override
    public NewsEntity getNewsEntityForId(final int ID, final IRepoListener<NewsEntity> callback)
    {
        NewsEntity entity = mCache.getCachedDataFor(ID);
        if(entity == null)
        {
            //Query Server for file
            this.mNetwork.fetchJSONWithURL(String.format(Constants.NEWS_DETAILS_FOR_ID_URL,ID), new INetworkCallbackInterface<JSONObject>() {

                final WeakReference<IRepoListener<NewsEntity>> listener = new WeakReference<>(callback);
                final WeakReference<IConverter> weakConverter = new WeakReference<>(mConverter);

                @Override
                public void onSuccess(JSONObject result) {
                    IConverter converter = weakConverter.get();
                    if (converter != null) {
                        NewsEntity newsEntity = converter.convertToNewsEntity(result);
                        mCache.setNewsEntity(ID,newsEntity);
                        IRepoListener<NewsEntity> cb = listener.get();
                        if (cb != null) {
                            cb.accept(newsEntity);
                        }
                    }
                }

                @Override
                public void onError(Error error) {
                    IRepoListener cb = listener.get();
                    if (cb != null) {
                        cb.reject(error);
                    }
                }
            });
        }
        return entity;
    }

    @Override
    public NewsEntity getNewsEntityForId(int ID) {
        return mCache.getCachedDataFor(ID);
    }
}
