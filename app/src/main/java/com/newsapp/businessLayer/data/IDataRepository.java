package com.newsapp.businessLayer.data;

import com.newsapp.businessLayer.network.Error;
import com.newsapp.businessLayer.entity.NewsEntity;

import java.util.List;

/**
 * The DatRepository interface.
 * Allows data access through cache and network
 * Created by Faraz Khan.
 */


public interface IDataRepository {

    interface IRepoListener<T>{
        void accept(T data);
        void reject(Error error);
    }

    void getNewsList(final IRepoListener<List<Integer>> repo);
    NewsEntity getNewsEntityForId(int ID, IRepoListener<NewsEntity> callback);
    NewsEntity getNewsEntityForId(int ID);
}
