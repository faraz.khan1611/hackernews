package com.newsapp.businessLayer.data;

import com.newsapp.businessLayer.entity.NewsEntity;

import java.util.List;

/**
 * The Cache implementaion.
 * Stores obj for faster access
 * Created by Faraz Khan.
 */

interface ICache {

    void setCacheData(List<Integer> list);
    void setNewsEntity(int id, NewsEntity entity);
    NewsEntity getCachedDataFor(int id);
}
