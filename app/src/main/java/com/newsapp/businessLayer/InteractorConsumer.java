package com.newsapp.businessLayer;

import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;

import java.util.List;

public interface InteractorConsumer {
    void accept(List<Integer> data);
    void accept(int position, NewsEntity entity);
    void reject(int position,Error error);
    void reject(Error error);
}
