package com.newsapp.businessLayer.network;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The AsyncOperation implementation.
 * Provides threading support
 * Created by Faraz Khan.
 */

class AsyncOperation {

    private final static ExecutorService mExecutorService = Executors.newCachedThreadPool();

    static void postAsync(final Runnable runnable) {
        mExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                runnable.run();
            }
        });
    }
}
