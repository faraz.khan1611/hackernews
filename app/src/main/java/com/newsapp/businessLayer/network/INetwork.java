package com.newsapp.businessLayer.network;

import org.json.JSONArray;
import org.json.JSONObject;

public interface INetwork {
    void fetchJSONWithURL(final String URL, final INetworkCallbackInterface<JSONObject> networkCallback);

    void fetchJSONArrayWithURL(String URL, INetworkCallbackInterface<JSONArray> networkCallback);
}