package com.newsapp.businessLayer.network;

/**
 * Created by Faraz Khan.
 */

@SuppressWarnings("unused")
public class Error {
    private final String message;

    public Error(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
