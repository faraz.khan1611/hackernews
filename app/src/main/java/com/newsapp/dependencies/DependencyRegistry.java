package com.newsapp.dependencies;

import com.newsapp.businessLayer.converter.ConverterImpl;
import com.newsapp.businessLayer.data.IDataRepository;
import com.newsapp.businessLayer.IInteractorInterface;
import com.newsapp.businessLayer.Interactor;
import com.newsapp.businessLayer.network.INetwork;
import com.newsapp.businessLayer.network.NetworkImpl;
import com.newsapp.businessLayer.data.DataRepositoryImpl;
import com.newsapp.router.Router;
import com.newsapp.view.activity.CommentsActivity;
import com.newsapp.view.activity.MainActivity;

import org.jetbrains.annotations.NotNull;

import com.newsapp.businessLayer.converter.IConverter;

/**
 * Dependency registry for handling singleton creations
 * Created by Faraz Khan.
 */

public class DependencyRegistry {
    private static class Singleton {
        private static final DependencyRegistry INSTANCE = new DependencyRegistry();
    }

    public static DependencyRegistry getInstance() {
        return DependencyRegistry.Singleton.INSTANCE;
    }

    //region Singletons

    private final IConverter mConvertor = createConverter();

    private IConverter createConverter() {
        return new ConverterImpl();
    }

    private final INetwork mNetworkLayer = new NetworkImpl();

    private final IDataRepository mRepository = createRepository();

    private IDataRepository createRepository() {
        return new DataRepositoryImpl(mNetworkLayer, mConvertor);
    }

    private final IInteractorInterface mInteractor = createInteractor();

    private IInteractorInterface createInteractor() {
        return new Interactor(mRepository);
    }

    //endregion

    //region Coordinators

    private final Router mRouter = new Router();

    //endregion

    //region Injection Methods
    public void inject(@NotNull MainActivity activity) {
        activity.configureInteractor(mInteractor,mRouter);
    }

    public void inject(@NotNull CommentsActivity activity) {
        activity.configureInteractor(mInteractor);
    }
    //endregion


}
