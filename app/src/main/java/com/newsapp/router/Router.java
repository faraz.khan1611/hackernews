package com.newsapp.router;

import android.content.Context;
import android.content.Intent;

import com.newsapp.helper.Constants;
import com.newsapp.view.activity.CommentsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Faraz Khan.
 */

public class Router {

    public void openCommentsActivity(Context context,int id, List<Integer> kids) {
        Intent commentsActivity = new Intent(context, CommentsActivity.class);
        commentsActivity.putExtra(Constants.NEWS_ID,id);
        commentsActivity.putIntegerArrayListExtra(Constants.COMMENTS_LIST, (ArrayList<Integer>) kids);
        context.startActivity(commentsActivity);
    }
}
