package com.newsapp.view.viewModels;

import com.newsapp.businessLayer.network.Error;

/**
 * Created by Faraz Khan.
 */

public class NewsEntityModel<T> {
    private int position;
    private final NewsEntityState state;
    private final T entity;
    private final Error error;

    public int getPosition() {
        return position;
    }

    public NewsEntityState getState() {
        return state;
    }

    public T getEntity() {
        return entity;
    }

    public Error getError() {
        return error;
    }

    public NewsEntityModel(int position, NewsEntityState state, T entity, Error error) {
        this.position = position;
        this.state = state;
        this.entity = entity;
        this.error = error;
    }

    public NewsEntityModel(NewsEntityState state, T entity, Error error) {
        this.state = state;
        this.entity = entity;
        this.error = error;
    }
}
