package com.newsapp.view.viewModels;

import android.os.Handler;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.IInteractorInterface;
import com.newsapp.businessLayer.InteractorConsumer;
import com.newsapp.businessLayer.network.Error;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Faraz Khan.
 */

public class NewsDataViewModel extends ViewModel implements InteractorConsumer {

    private final MutableLiveData<NewsEntityModel<NewsEntity>> newsEntityLiveData = new MutableLiveData<>();
    private final MutableLiveData<NewsEntityModel<List<Integer>>> newsListLiveData = new MutableLiveData<>();
    private final Handler handler = new Handler();
    private boolean mIsInitialSyncDone = false;

    public MutableLiveData<NewsEntityModel<NewsEntity>> getNewsEntityLiveData() {
        return newsEntityLiveData;
    }

    public MutableLiveData<NewsEntityModel<List<Integer>>> getNewsListLiveData() {
        return newsListLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        handler.removeCallbacksAndMessages(null);
    }

    public NewsEntity getNewsEntityForPositionAndID(final int position, int newsID, IInteractorInterface interactor) {
        return interactor.getDetailedNewsForPositionAndID(position, newsID, this);
    }

    public void initLoadNewsList(IInteractorInterface interactor) {
        if(!mIsInitialSyncDone) {
            mIsInitialSyncDone = true;
            interactor.loadNewsList(this);
        }
    }

    public void forceLoadNewsList(IInteractorInterface interactor) {
        interactor.loadNewsList(this);
    }

    //region InteractorConsumer implementation
    @Override
    public void accept(final int position, final NewsEntity entity) {
        handler.post(new Runnable() {
            final WeakReference<MutableLiveData<NewsEntityModel<NewsEntity>>> ref = new WeakReference<>(newsEntityLiveData);
            @Override
            public void run() {
                if(ref.get() != null) {
                    newsEntityLiveData.setValue(new NewsEntityModel<>(position, NewsEntityState.SUCCESS, entity, null));
                }
            }
        });
    }

    @Override
    public void reject(final int position, final Error error) {
        handler.post(new Runnable() {
            final WeakReference<MutableLiveData<NewsEntityModel<NewsEntity>>> ref = new WeakReference<>(newsEntityLiveData);
            @Override
            public void run() {
                if(ref.get() != null) {
                    newsEntityLiveData.setValue(new NewsEntityModel<NewsEntity>(position, NewsEntityState.FAIL, null, error));
                }
            }
        });
    }

    @Override
    public void accept(List<Integer> data) {
        newsListLiveData.postValue(new NewsEntityModel<>(NewsEntityState.SUCCESS, data, null));
    }

    @Override
    public void reject(Error error) {
        newsListLiveData.postValue(new NewsEntityModel<List<Integer>>(NewsEntityState.FAIL, null, error));
    }
    //endregion
}
