package com.newsapp.view.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.news.sample.R;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.IInteractorInterface;
import com.newsapp.dependencies.DependencyRegistry;
import com.newsapp.router.Router;
import com.newsapp.view.viewModels.NewsDataViewModel;
import com.newsapp.view.viewModels.NewsEntityModel;
import com.newsapp.view.viewModels.NewsEntityState;

import java.util.List;

/**
 * Created by Faraz Khan.
 */

public class MainActivity extends AppCompatActivity implements NewsListRecyclerViewAdapter.NewsListParentListener {

    private IInteractorInterface mInteractor;
    private Router mRouter;
    private NewsListRecyclerViewAdapter adapter;
    private RecyclerView newsRecycleView;
    private NewsDataViewModel mViewModel;
    private ProgressBar progressBar;
    private TextView resultcount;
    private TextView loaderText;
    private Toast toast;
    private SwipeRefreshLayout pullToRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        newsRecycleView = findViewById(R.id.recyclerViewNews);
        progressBar = findViewById(R.id.progressBar);
        resultcount = findViewById(R.id.resultcount);
        loaderText = findViewById(R.id.loading);
        mViewModel = ViewModelProviders.of(this).get(NewsDataViewModel.class);
        DependencyRegistry.getInstance().inject(this);
    }

    //region Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region Initialize
    public void configureInteractor(IInteractorInterface mInteractor, Router mRouter) {
        this.mInteractor = mInteractor;
        this.mRouter = mRouter;
        loadInitialView();
        initObservers();
    }

    private void initObservers() {
        mViewModel.getNewsListLiveData().observe(this, new Observer<NewsEntityModel<List<Integer>>>() {
            @Override
            public void onChanged(NewsEntityModel<List<Integer>> listNewsEntityModel) {
                if (listNewsEntityModel.getState() == NewsEntityState.SUCCESS) {
                    adapter.loadNewsList(listNewsEntityModel.getEntity());
                } else {
                    //Handle error scenarios
                    if (toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(getBaseContext(), "Error found :: " + listNewsEntityModel.getError().getMessage(), Toast.LENGTH_SHORT);
                    toast.show();
                }
                hideProgress();
            }
        });

        mViewModel.getNewsEntityLiveData().observe(this, new Observer<NewsEntityModel<NewsEntity>>() {
            @Override
            public void onChanged(NewsEntityModel<NewsEntity> newsEntityNewsEntityModel) {
                if (newsEntityNewsEntityModel.getState() == NewsEntityState.SUCCESS) {
                    adapter.notifyItemChanged(newsEntityNewsEntityModel.getPosition());
                } else {
                    //Handle error scenarios
                    if (toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(getBaseContext(), "Error found :: " + newsEntityNewsEntityModel.getError().getMessage(), Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
    }

    @SuppressLint("DefaultLocale")
    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            loaderText.setVisibility(View.GONE);
            resultcount.setText(String.format("%d results", adapter.getItemCount()));
        }
    }

    private void loadInitialView() {
        adapter = new NewsListRecyclerViewAdapter(this, false);
        newsRecycleView.setAdapter(adapter);
        if (mViewModel != null) {
            mViewModel.initLoadNewsList(mInteractor);
        }
        pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mViewModel != null) {
                    mViewModel.forceLoadNewsList(mInteractor);
                }
                pullToRefresh.setRefreshing(false);
            }
        });
    }
    //endregion


    //region ParentListener implementation
    @Override
    public NewsEntity getNewsEntityForPositionAndId(int position, int ID) {
        if (mViewModel != null) {
            return mViewModel.getNewsEntityForPositionAndID(position, ID, mInteractor);
        }
        return null;
    }

    @Override
    public void onItemclick(int id, List<Integer> kids) {
        if (mRouter != null) {
            mRouter.openCommentsActivity(MainActivity.this, id, kids);
        }
    }
    //endregion

}
