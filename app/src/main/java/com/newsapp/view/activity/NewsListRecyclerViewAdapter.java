package com.newsapp.view.activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.news.sample.R;
import com.newsapp.businessLayer.entity.NewsEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Faraz Khan.
 */

public class NewsListRecyclerViewAdapter extends RecyclerView.Adapter<NewsListRecyclerViewAdapter.ViewHolder> {

    private final List<Integer> dataModelList = new ArrayList<>();
    private final int TYPE_NEWS = 1;
    private final boolean isCommentsScreen;
    private final NewsListParentListener listener;
    public interface NewsListParentListener{
        NewsEntity getNewsEntityForPositionAndId(int position,int ID);
        void onItemclick(int id, List<Integer> kids);
    }

    NewsListRecyclerViewAdapter(NewsListParentListener listener, boolean isCommentsScreen) {
        this.isCommentsScreen = isCommentsScreen;
        this.listener = listener;
    }

    void loadNewsList(List<Integer> list)
    {
        dataModelList.clear();
        dataModelList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NewsListRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        if(viewType == TYPE_NEWS) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_row, parent, false);
        }
        else{
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comments_row, parent, false);
        }
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsListRecyclerViewAdapter.ViewHolder holder, int position) {
        if(listener != null) {
            NewsEntity entity = listener.getNewsEntityForPositionAndId(position,dataModelList.get(position));
            if (entity != null) {
                holder.hideProgress();
                if(holder.getItemViewType() == TYPE_NEWS) {
                    holder.bindNewsView(entity, listener);
                }
                else
                {
                    holder.bindCommentsView(entity);
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        int TYPE_COMMENTS = 2;
        return isCommentsScreen? TYPE_COMMENTS :TYPE_NEWS;
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView title;
        final TextView subtile;
        final TextView hostname;
        final ProgressBar progress;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.news_title);
            subtile = itemView.findViewById(R.id.news_subtitle);
            hostname = itemView.findViewById(R.id.news_hostname);
            progress = itemView.findViewById(R.id.progressBar_loader);
        }

        private void hideProgress() {
            if (progress != null) {
                progress.setVisibility(View.GONE);
            }
        }

        void bindNewsView(final NewsEntity entity, final NewsListParentListener listener)
        {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemclick(entity.getId(),entity.getKids());
                }
            });
            String parentSubtitle = this.subtile.getContext().getResources().getString(R.string.textview_subtitle_news_format);
            this.title.setText(entity.getTitle());
            this.hostname.setText(String.format(" -%s", entity.getHostname()));
            String formatedString = String.format(parentSubtitle,entity.getScore(),entity.getBy(),entity.getTimeToDisplay(),entity.getDescendants());
            this.subtile.setText(formatedString);

        }

        void bindCommentsView(final NewsEntity entity)
        {
            String parentSubtitle = this.subtile.getContext().getResources().getString(R.string.textview_subtitle_comments_format);
            this.title.setText(entity.getText());
            this.hostname.setText(this.hostname.getContext().getString(R.string.reply));
            String formatedString = String.format(parentSubtitle,entity.getBy(),entity.getTimeToDisplay());
            this.subtile.setText(formatedString);

        }
    }
}
