package com.newsapp.view.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.news.sample.R;
import com.newsapp.businessLayer.IInteractorInterface;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.dependencies.DependencyRegistry;
import com.newsapp.helper.Constants;
import com.newsapp.view.viewModels.NewsDataViewModel;
import com.newsapp.view.viewModels.NewsEntityModel;
import com.newsapp.view.viewModels.NewsEntityState;

import java.util.List;
/**
 * Created by Faraz Khan.
 */

public class CommentsActivity extends AppCompatActivity implements NewsListRecyclerViewAdapter.NewsListParentListener {
    private IInteractorInterface mInteractor;
    private NewsListRecyclerViewAdapter adapter;
    private RecyclerView newsRecycleView;
    private NewsDataViewModel mViewModel;
    private ProgressBar progressBar;
    private TextView loaderText;
    private TextView title;
    private TextView subtile;
    private TextView hostname;
    private int PARENTID;
    private List<Integer> commentsIDList;
    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        title = findViewById(R.id.news_title);
        subtile = findViewById(R.id.news_subtitle);
        hostname = findViewById(R.id.news_hostname);
        newsRecycleView = findViewById(R.id.recyclerViewNews);
        progressBar = findViewById(R.id.progressBar);
        loaderText = findViewById(R.id.loading);
        mViewModel = ViewModelProviders.of(this).get(NewsDataViewModel.class);
        if (getIntent().hasExtra(Constants.NEWS_ID)) {
            PARENTID = getIntent().getIntExtra(Constants.NEWS_ID, -1);
        }
        if (getIntent().hasExtra(Constants.COMMENTS_LIST)) {
            commentsIDList = getIntent().getIntegerArrayListExtra(Constants.COMMENTS_LIST);
        }
        DependencyRegistry.getInstance().inject(this);
    }

    //region Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region Initialize
    public void configureInteractor(IInteractorInterface mInteractor) {
        this.mInteractor = mInteractor;
        loadInitialView();
        initObservers();
    }

    private void initObservers() {
        adapter.loadNewsList(commentsIDList);
        hideProgress();

        mViewModel.getNewsEntityLiveData().observe(this, new Observer<NewsEntityModel<NewsEntity>>() {
            @Override
            public void onChanged(NewsEntityModel<NewsEntity> newsEntityNewsEntityModel) {
                if (newsEntityNewsEntityModel.getState() == NewsEntityState.SUCCESS) {
                    adapter.notifyItemChanged(newsEntityNewsEntityModel.getPosition());
                } else {
                    //Handle error scenarios
                    if (toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(getBaseContext(), "Error found :: " + newsEntityNewsEntityModel.getError().getMessage(), Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
    }

    @SuppressLint("DefaultLocale")
    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            loaderText.setVisibility(View.GONE);
        }
    }

    private void loadInitialView() {
        adapter = new NewsListRecyclerViewAdapter(this, true);
        newsRecycleView.setAdapter(adapter);
        prepareParentCard();
    }

    private void prepareParentCard() {
        if (PARENTID != -1) {
            NewsEntity entity = mInteractor.getDetailedNewsForID(PARENTID);
            if (entity != null) {
                String parentSubtitle = this.subtile.getContext().getResources().getString(R.string.textview_subtitle_news_format);
                this.title.setText(entity.getTitle());
                this.hostname.setText(String.format(" -%s", entity.getHostname()));
                String formatedString = String.format(parentSubtitle, entity.getScore(), entity.getBy(), entity.getTimeToDisplay(), entity.getDescendants());
                this.subtile.setText(formatedString);
            }
        }
    }
    //endregion


    //region ParentListener implementation
    @Override
    public NewsEntity getNewsEntityForPositionAndId(int position, int ID) {
        return mViewModel.getNewsEntityForPositionAndID(position, ID, mInteractor);
    }

    @Override
    public void onItemclick(int id, List<Integer> kids) {
    }
    //endregion

}
